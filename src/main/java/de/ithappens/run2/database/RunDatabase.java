package de.ithappens.run2.database;

import java.sql.SQLException;

import org.apache.derby.jdbc.ClientConnectionPoolDataSource;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import de.ithappens.run2.database.model.engine.AgeClassesDefinition_engine;
import de.ithappens.run2.database.model.engine.Competition_engine;
import de.ithappens.run2.database.model.engine.Discipline_engine;

public class RunDatabase {

	private DSLContext dsl = null;
	private String appNameAndVersion, currentUser;

	private Competition_engine competition_engine;
	private AgeClassesDefinition_engine ageClassesDefinition_engine;
	private Discipline_engine discipline_engine;

	public RunDatabase(DataSourceConnectionData config, String _appNameAndVersion, String _currentUser)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		appNameAndVersion = _appNameAndVersion;
		currentUser = _currentUser;
		ClientConnectionPoolDataSource dbDataSource = new ClientConnectionPoolDataSource();
		dbDataSource.setServerName(config.serverName);
		dbDataSource.setDatabaseName(config.schemaName);
        dbDataSource.setUser(config.account);
        dbDataSource.setPassword(config.password);
        dsl = DSL.using(dbDataSource, SQLDialect.DERBY);
	}
	
	public boolean isConnected() {
		boolean connected = false;
		try {
			connected = dsl.diagnosticsDataSource().getConnection() != null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			;
		}
		return connected;
	}

	public Competition_engine COMPETITION_engine() {
		return competition_engine != null ? competition_engine
				: (competition_engine = new Competition_engine(dsl.configuration()));
	}

	public Discipline_engine DISCIPLINE_engine() {
		return discipline_engine != null ? discipline_engine
				: (discipline_engine = new Discipline_engine(dsl.configuration()));
	}

	public AgeClassesDefinition_engine AGE_CLASSES_DEFINITION_engine() {
		return ageClassesDefinition_engine != null ? ageClassesDefinition_engine
				: (ageClassesDefinition_engine = new AgeClassesDefinition_engine(dsl.configuration()));
	}

}
