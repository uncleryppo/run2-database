package de.ithappens.run2.database;

public class DataSourceConnectionData {

	public String name, driver, serverName, schemaName, account, password;

	public DataSourceConnectionData(String _name, String _driver, String _serverName, String _schemaName,
			String _account, String _password) {
		name = _name;
		driver = _driver;
		serverName = _serverName;
		schemaName = _schemaName;
		account = _account;
		password = _password;
	}

}