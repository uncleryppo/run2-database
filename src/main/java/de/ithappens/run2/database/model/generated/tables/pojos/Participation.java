/*
 * This file is generated by jOOQ.
 */
package de.ithappens.run2.database.model.generated.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Participation implements Serializable {

    private static final long serialVersionUID = -1967931418;

    private Integer   id;
    private String    resulttime;
    private String    participationParticipationNumber;
    private Short     feePaid;
    private Short     certificationHandedover;
    private Short     noncompetitive;
    private Integer   relatedcompetitionid;
    private Integer   relatedcontactid;
    private Integer   relateddisciplineid;
    private Integer   rank;
    private Integer   participationAgeclassRank;
    private Integer   participationGenderRank;
    private Integer   participationGenderAgeclassRank;
    private String    participationComment;
    private Integer   participationDonationHospiz;
    private Short     participationRegisteredOnline;
    private String    importFingerprint;
    private Timestamp creationdate;
    private String    creator;
    private Timestamp changedate;
    private String    changer;
    private Integer   version;
    private Integer   relatedteamid;
    private Short     canceled;
    private Short     notStarted;

    public Participation() {}

    public Participation(Participation value) {
        this.id = value.id;
        this.resulttime = value.resulttime;
        this.participationParticipationNumber = value.participationParticipationNumber;
        this.feePaid = value.feePaid;
        this.certificationHandedover = value.certificationHandedover;
        this.noncompetitive = value.noncompetitive;
        this.relatedcompetitionid = value.relatedcompetitionid;
        this.relatedcontactid = value.relatedcontactid;
        this.relateddisciplineid = value.relateddisciplineid;
        this.rank = value.rank;
        this.participationAgeclassRank = value.participationAgeclassRank;
        this.participationGenderRank = value.participationGenderRank;
        this.participationGenderAgeclassRank = value.participationGenderAgeclassRank;
        this.participationComment = value.participationComment;
        this.participationDonationHospiz = value.participationDonationHospiz;
        this.participationRegisteredOnline = value.participationRegisteredOnline;
        this.importFingerprint = value.importFingerprint;
        this.creationdate = value.creationdate;
        this.creator = value.creator;
        this.changedate = value.changedate;
        this.changer = value.changer;
        this.version = value.version;
        this.relatedteamid = value.relatedteamid;
        this.canceled = value.canceled;
        this.notStarted = value.notStarted;
    }

    public Participation(
        Integer   id,
        String    resulttime,
        String    participationParticipationNumber,
        Short     feePaid,
        Short     certificationHandedover,
        Short     noncompetitive,
        Integer   relatedcompetitionid,
        Integer   relatedcontactid,
        Integer   relateddisciplineid,
        Integer   rank,
        Integer   participationAgeclassRank,
        Integer   participationGenderRank,
        Integer   participationGenderAgeclassRank,
        String    participationComment,
        Integer   participationDonationHospiz,
        Short     participationRegisteredOnline,
        String    importFingerprint,
        Timestamp creationdate,
        String    creator,
        Timestamp changedate,
        String    changer,
        Integer   version,
        Integer   relatedteamid,
        Short     canceled,
        Short     notStarted
    ) {
        this.id = id;
        this.resulttime = resulttime;
        this.participationParticipationNumber = participationParticipationNumber;
        this.feePaid = feePaid;
        this.certificationHandedover = certificationHandedover;
        this.noncompetitive = noncompetitive;
        this.relatedcompetitionid = relatedcompetitionid;
        this.relatedcontactid = relatedcontactid;
        this.relateddisciplineid = relateddisciplineid;
        this.rank = rank;
        this.participationAgeclassRank = participationAgeclassRank;
        this.participationGenderRank = participationGenderRank;
        this.participationGenderAgeclassRank = participationGenderAgeclassRank;
        this.participationComment = participationComment;
        this.participationDonationHospiz = participationDonationHospiz;
        this.participationRegisteredOnline = participationRegisteredOnline;
        this.importFingerprint = importFingerprint;
        this.creationdate = creationdate;
        this.creator = creator;
        this.changedate = changedate;
        this.changer = changer;
        this.version = version;
        this.relatedteamid = relatedteamid;
        this.canceled = canceled;
        this.notStarted = notStarted;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResulttime() {
        return this.resulttime;
    }

    public void setResulttime(String resulttime) {
        this.resulttime = resulttime;
    }

    public String getParticipationParticipationNumber() {
        return this.participationParticipationNumber;
    }

    public void setParticipationParticipationNumber(String participationParticipationNumber) {
        this.participationParticipationNumber = participationParticipationNumber;
    }

    public Short getFeePaid() {
        return this.feePaid;
    }

    public void setFeePaid(Short feePaid) {
        this.feePaid = feePaid;
    }

    public Short getCertificationHandedover() {
        return this.certificationHandedover;
    }

    public void setCertificationHandedover(Short certificationHandedover) {
        this.certificationHandedover = certificationHandedover;
    }

    public Short getNoncompetitive() {
        return this.noncompetitive;
    }

    public void setNoncompetitive(Short noncompetitive) {
        this.noncompetitive = noncompetitive;
    }

    public Integer getRelatedcompetitionid() {
        return this.relatedcompetitionid;
    }

    public void setRelatedcompetitionid(Integer relatedcompetitionid) {
        this.relatedcompetitionid = relatedcompetitionid;
    }

    public Integer getRelatedcontactid() {
        return this.relatedcontactid;
    }

    public void setRelatedcontactid(Integer relatedcontactid) {
        this.relatedcontactid = relatedcontactid;
    }

    public Integer getRelateddisciplineid() {
        return this.relateddisciplineid;
    }

    public void setRelateddisciplineid(Integer relateddisciplineid) {
        this.relateddisciplineid = relateddisciplineid;
    }

    public Integer getRank() {
        return this.rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getParticipationAgeclassRank() {
        return this.participationAgeclassRank;
    }

    public void setParticipationAgeclassRank(Integer participationAgeclassRank) {
        this.participationAgeclassRank = participationAgeclassRank;
    }

    public Integer getParticipationGenderRank() {
        return this.participationGenderRank;
    }

    public void setParticipationGenderRank(Integer participationGenderRank) {
        this.participationGenderRank = participationGenderRank;
    }

    public Integer getParticipationGenderAgeclassRank() {
        return this.participationGenderAgeclassRank;
    }

    public void setParticipationGenderAgeclassRank(Integer participationGenderAgeclassRank) {
        this.participationGenderAgeclassRank = participationGenderAgeclassRank;
    }

    public String getParticipationComment() {
        return this.participationComment;
    }

    public void setParticipationComment(String participationComment) {
        this.participationComment = participationComment;
    }

    public Integer getParticipationDonationHospiz() {
        return this.participationDonationHospiz;
    }

    public void setParticipationDonationHospiz(Integer participationDonationHospiz) {
        this.participationDonationHospiz = participationDonationHospiz;
    }

    public Short getParticipationRegisteredOnline() {
        return this.participationRegisteredOnline;
    }

    public void setParticipationRegisteredOnline(Short participationRegisteredOnline) {
        this.participationRegisteredOnline = participationRegisteredOnline;
    }

    public String getImportFingerprint() {
        return this.importFingerprint;
    }

    public void setImportFingerprint(String importFingerprint) {
        this.importFingerprint = importFingerprint;
    }

    public Timestamp getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(Timestamp creationdate) {
        this.creationdate = creationdate;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Timestamp getChangedate() {
        return this.changedate;
    }

    public void setChangedate(Timestamp changedate) {
        this.changedate = changedate;
    }

    public String getChanger() {
        return this.changer;
    }

    public void setChanger(String changer) {
        this.changer = changer;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getRelatedteamid() {
        return this.relatedteamid;
    }

    public void setRelatedteamid(Integer relatedteamid) {
        this.relatedteamid = relatedteamid;
    }

    public Short getCanceled() {
        return this.canceled;
    }

    public void setCanceled(Short canceled) {
        this.canceled = canceled;
    }

    public Short getNotStarted() {
        return this.notStarted;
    }

    public void setNotStarted(Short notStarted) {
        this.notStarted = notStarted;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Participation (");

        sb.append(id);
        sb.append(", ").append(resulttime);
        sb.append(", ").append(participationParticipationNumber);
        sb.append(", ").append(feePaid);
        sb.append(", ").append(certificationHandedover);
        sb.append(", ").append(noncompetitive);
        sb.append(", ").append(relatedcompetitionid);
        sb.append(", ").append(relatedcontactid);
        sb.append(", ").append(relateddisciplineid);
        sb.append(", ").append(rank);
        sb.append(", ").append(participationAgeclassRank);
        sb.append(", ").append(participationGenderRank);
        sb.append(", ").append(participationGenderAgeclassRank);
        sb.append(", ").append(participationComment);
        sb.append(", ").append(participationDonationHospiz);
        sb.append(", ").append(participationRegisteredOnline);
        sb.append(", ").append(importFingerprint);
        sb.append(", ").append(creationdate);
        sb.append(", ").append(creator);
        sb.append(", ").append(changedate);
        sb.append(", ").append(changer);
        sb.append(", ").append(version);
        sb.append(", ").append(relatedteamid);
        sb.append(", ").append(canceled);
        sb.append(", ").append(notStarted);

        sb.append(")");
        return sb.toString();
    }
}
