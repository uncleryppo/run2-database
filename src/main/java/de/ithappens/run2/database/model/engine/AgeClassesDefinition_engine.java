package de.ithappens.run2.database.model.engine;

import java.sql.Timestamp;
import java.util.List;

import org.jooq.Configuration;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.database.model.generated.tables.daos.AgeclassesdefinitionDao;
import de.ithappens.run2.database.model.generated.tables.pojos.Ageclassesdefinition;

public class AgeClassesDefinition_engine extends EngineInterface<Ageclassesdefinition> {

	public AgeClassesDefinition_engine(Configuration configuration) {
		super(configuration);
	}

	@Override
	protected AgeclassesdefinitionDao createDao(Configuration configuration) {
		return new AgeclassesdefinitionDao(configuration);
	}

	@Override
	public AgeclassesdefinitionDao DAO() {
		return (AgeclassesdefinitionDao) super.DAO();
	}

	@Override
	public void update(Ageclassesdefinition model) {
		if (model != null) {
			model.setVersion(model.getVersion() + 1);
			model.setChangedate(new Timestamp(System.currentTimeMillis()));
			model.setChanger(UserContext.getInstance().getOperatingSystemEnvironment().getUsername());
			DAO().update(model);
		}
	}

	@Override
	public List<Ageclassesdefinition> findAll() {
		return DAO().findAll();
	}

}
