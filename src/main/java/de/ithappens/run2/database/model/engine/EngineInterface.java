package de.ithappens.run2.database.model.engine;

import java.util.List;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;

public abstract class EngineInterface<T> {
	
	private final DAOImpl<?, ?, ?> dao;
	
	public EngineInterface(Configuration configuration) {
        dao = createDao(configuration);
    }
	
	protected abstract DAOImpl<?, ?, ?> createDao(Configuration configuration);
	
	/**
     * Overwrite and cast this to get your specific DAO type
     * @return the direct access object (=DAO)
     */
    public DAOImpl<?, ?, ?> DAO() {
    	return dao;
    }
    
    public long countAll() {
		return DAO().count();
	}

	public abstract void update(T model);

	public abstract List<T> findAll();

}
