/*
 * This file is generated by jOOQ.
 */
package de.ithappens.run2.database.model.generated.tables.daos;


import de.ithappens.run2.database.model.generated.tables.Ageclass;
import de.ithappens.run2.database.model.generated.tables.records.AgeclassRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AgeclassDao extends DAOImpl<AgeclassRecord, de.ithappens.run2.database.model.generated.tables.pojos.Ageclass, Integer> {

    /**
     * Create a new AgeclassDao without any configuration
     */
    public AgeclassDao() {
        super(Ageclass.AGECLASS, de.ithappens.run2.database.model.generated.tables.pojos.Ageclass.class);
    }

    /**
     * Create a new AgeclassDao with an attached configuration
     */
    public AgeclassDao(Configuration configuration) {
        super(Ageclass.AGECLASS, de.ithappens.run2.database.model.generated.tables.pojos.Ageclass.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(de.ithappens.run2.database.model.generated.tables.pojos.Ageclass object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>ID IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchById(Integer... values) {
        return fetch(Ageclass.AGECLASS.ID, values);
    }

    /**
     * Fetch a unique record that has <code>ID = value</code>
     */
    public de.ithappens.run2.database.model.generated.tables.pojos.Ageclass fetchOneById(Integer value) {
        return fetchOne(Ageclass.AGECLASS.ID, value);
    }

    /**
     * Fetch records that have <code>AGECLASS_TITLE IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByAgeclassTitle(String... values) {
        return fetch(Ageclass.AGECLASS.AGECLASS_TITLE, values);
    }

    /**
     * Fetch records that have <code>AGECLASS_YEAR_FROM IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByAgeclassYearFrom(Integer... values) {
        return fetch(Ageclass.AGECLASS.AGECLASS_YEAR_FROM, values);
    }

    /**
     * Fetch records that have <code>AGECLASS_YEAR_TO IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByAgeclassYearTo(Integer... values) {
        return fetch(Ageclass.AGECLASS.AGECLASS_YEAR_TO, values);
    }

    /**
     * Fetch records that have <code>AGECLASSESDEFINITION IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByAgeclassesdefinition(Integer... values) {
        return fetch(Ageclass.AGECLASS.AGECLASSESDEFINITION, values);
    }

    /**
     * Fetch records that have <code>IMPORT_FINGERPRINT IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByImportFingerprint(String... values) {
        return fetch(Ageclass.AGECLASS.IMPORT_FINGERPRINT, values);
    }

    /**
     * Fetch records that have <code>CREATIONDATE IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByCreationdate(Timestamp... values) {
        return fetch(Ageclass.AGECLASS.CREATIONDATE, values);
    }

    /**
     * Fetch records that have <code>CREATOR IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByCreator(String... values) {
        return fetch(Ageclass.AGECLASS.CREATOR, values);
    }

    /**
     * Fetch records that have <code>CHANGEDATE IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByChangedate(Timestamp... values) {
        return fetch(Ageclass.AGECLASS.CHANGEDATE, values);
    }

    /**
     * Fetch records that have <code>CHANGER IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByChanger(String... values) {
        return fetch(Ageclass.AGECLASS.CHANGER, values);
    }

    /**
     * Fetch records that have <code>VERSION IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByVersion(Integer... values) {
        return fetch(Ageclass.AGECLASS.VERSION, values);
    }

    /**
     * Fetch records that have <code>SHORT_NAME IN (values)</code>
     */
    public List<de.ithappens.run2.database.model.generated.tables.pojos.Ageclass> fetchByShortName(String... values) {
        return fetch(Ageclass.AGECLASS.SHORT_NAME, values);
    }
}
