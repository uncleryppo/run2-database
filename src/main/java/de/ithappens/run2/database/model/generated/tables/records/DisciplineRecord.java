/*
 * This file is generated by jOOQ.
 */
package de.ithappens.run2.database.model.generated.tables.records;


import de.ithappens.run2.database.model.generated.tables.Discipline;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record13;
import org.jooq.Row13;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DisciplineRecord extends UpdatableRecordImpl<DisciplineRecord> implements Record13<Integer, String, String, Timestamp, String, Integer, String, Timestamp, String, Timestamp, String, Integer, String> {

    private static final long serialVersionUID = -2025485346;

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.ID</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.ID</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.NAME</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.NAME</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.LENGTH</code>.
     */
    public void setLength(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.LENGTH</code>.
     */
    public String getLength() {
        return (String) get(2);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.START</code>.
     */
    public void setStart(Timestamp value) {
        set(3, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.START</code>.
     */
    public Timestamp getStart() {
        return (Timestamp) get(3);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.DISCIPLINE_TIMEFORMAT</code>.
     */
    public void setDisciplineTimeformat(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.DISCIPLINE_TIMEFORMAT</code>.
     */
    public String getDisciplineTimeformat() {
        return (String) get(4);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.PRICE_IN_EUROCENT</code>.
     */
    public void setPriceInEurocent(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.PRICE_IN_EUROCENT</code>.
     */
    public Integer getPriceInEurocent() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.IMPORT_FINGERPRINT</code>.
     */
    public void setImportFingerprint(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.IMPORT_FINGERPRINT</code>.
     */
    public String getImportFingerprint() {
        return (String) get(6);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.CREATIONDATE</code>.
     */
    public void setCreationdate(Timestamp value) {
        set(7, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.CREATIONDATE</code>.
     */
    public Timestamp getCreationdate() {
        return (Timestamp) get(7);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.CREATOR</code>.
     */
    public void setCreator(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.CREATOR</code>.
     */
    public String getCreator() {
        return (String) get(8);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.CHANGEDATE</code>.
     */
    public void setChangedate(Timestamp value) {
        set(9, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.CHANGEDATE</code>.
     */
    public Timestamp getChangedate() {
        return (Timestamp) get(9);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.CHANGER</code>.
     */
    public void setChanger(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.CHANGER</code>.
     */
    public String getChanger() {
        return (String) get(10);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.VERSION</code>.
     */
    public void setVersion(Integer value) {
        set(11, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.VERSION</code>.
     */
    public Integer getVersion() {
        return (Integer) get(11);
    }

    /**
     * Setter for <code>Y3JRUN.DISCIPLINE.SHORT_NAME</code>.
     */
    public void setShortName(String value) {
        set(12, value);
    }

    /**
     * Getter for <code>Y3JRUN.DISCIPLINE.SHORT_NAME</code>.
     */
    public String getShortName() {
        return (String) get(12);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record13 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row13<Integer, String, String, Timestamp, String, Integer, String, Timestamp, String, Timestamp, String, Integer, String> fieldsRow() {
        return (Row13) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row13<Integer, String, String, Timestamp, String, Integer, String, Timestamp, String, Timestamp, String, Integer, String> valuesRow() {
        return (Row13) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Discipline.DISCIPLINE.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Discipline.DISCIPLINE.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Discipline.DISCIPLINE.LENGTH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field4() {
        return Discipline.DISCIPLINE.START;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return Discipline.DISCIPLINE.DISCIPLINE_TIMEFORMAT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return Discipline.DISCIPLINE.PRICE_IN_EUROCENT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return Discipline.DISCIPLINE.IMPORT_FINGERPRINT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field8() {
        return Discipline.DISCIPLINE.CREATIONDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return Discipline.DISCIPLINE.CREATOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field10() {
        return Discipline.DISCIPLINE.CHANGEDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field11() {
        return Discipline.DISCIPLINE.CHANGER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field12() {
        return Discipline.DISCIPLINE.VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field13() {
        return Discipline.DISCIPLINE.SHORT_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getLength();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component4() {
        return getStart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getDisciplineTimeformat();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component6() {
        return getPriceInEurocent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component7() {
        return getImportFingerprint();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component8() {
        return getCreationdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component9() {
        return getCreator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component10() {
        return getChangedate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component11() {
        return getChanger();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component12() {
        return getVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component13() {
        return getShortName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getLength();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value4() {
        return getStart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getDisciplineTimeformat();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getPriceInEurocent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getImportFingerprint();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value8() {
        return getCreationdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getCreator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value10() {
        return getChangedate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value11() {
        return getChanger();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value12() {
        return getVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value13() {
        return getShortName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value3(String value) {
        setLength(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value4(Timestamp value) {
        setStart(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value5(String value) {
        setDisciplineTimeformat(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value6(Integer value) {
        setPriceInEurocent(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value7(String value) {
        setImportFingerprint(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value8(Timestamp value) {
        setCreationdate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value9(String value) {
        setCreator(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value10(Timestamp value) {
        setChangedate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value11(String value) {
        setChanger(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value12(Integer value) {
        setVersion(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord value13(String value) {
        setShortName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisciplineRecord values(Integer value1, String value2, String value3, Timestamp value4, String value5, Integer value6, String value7, Timestamp value8, String value9, Timestamp value10, String value11, Integer value12, String value13) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached DisciplineRecord
     */
    public DisciplineRecord() {
        super(Discipline.DISCIPLINE);
    }

    /**
     * Create a detached, initialised DisciplineRecord
     */
    public DisciplineRecord(Integer id, String name, String length, Timestamp start, String disciplineTimeformat, Integer priceInEurocent, String importFingerprint, Timestamp creationdate, String creator, Timestamp changedate, String changer, Integer version, String shortName) {
        super(Discipline.DISCIPLINE);

        set(0, id);
        set(1, name);
        set(2, length);
        set(3, start);
        set(4, disciplineTimeformat);
        set(5, priceInEurocent);
        set(6, importFingerprint);
        set(7, creationdate);
        set(8, creator);
        set(9, changedate);
        set(10, changer);
        set(11, version);
        set(12, shortName);
    }
}
