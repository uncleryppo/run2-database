package de.ithappens.run2.database.model.engine;

import java.sql.Timestamp;
import java.util.List;

import org.jooq.Configuration;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.database.model.generated.tables.daos.CompetitionDao;
import de.ithappens.run2.database.model.generated.tables.pojos.Competition;

public class Competition_engine extends EngineInterface<Competition> {

	public Competition_engine(Configuration configuration) {
		super(configuration);
	}

	@Override
	protected CompetitionDao createDao(Configuration configuration) {
		return new CompetitionDao(configuration);
	}

	@Override
	public CompetitionDao DAO() {
		return (CompetitionDao) super.DAO();
	}

	@Override
	public void update(Competition competition) {
		if (competition != null) {
			competition.setVersion(competition.getVersion() + 1);
			competition.setChangedate(new Timestamp(System.currentTimeMillis()));
			competition.setChanger(UserContext.getInstance().getOperatingSystemEnvironment().getUsername());
			DAO().update(competition);
		}
	}

	@Override
	public List<Competition> findAll() {
		return DAO().findAll();
	}

}
