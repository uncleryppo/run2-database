package de.ithappens.run2.database.model.extended;

import java.sql.Timestamp;

public interface MetaDataIncluded {

	public abstract void setChanger(String changer);

	public abstract String getChanger();

	public abstract void setCreator(String creator);

	public abstract String getCreator();

	public abstract void setChangedate(Timestamp timestamp);

	public abstract Timestamp getChangedate();

	public abstract void setCreationdate(Timestamp timestamp);

	public abstract Timestamp getCreationdate();

	public abstract void setVersion(Integer version);

	public abstract Integer getVersion();

}
