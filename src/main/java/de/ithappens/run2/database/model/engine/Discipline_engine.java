package de.ithappens.run2.database.model.engine;

import java.sql.Timestamp;
import java.util.List;

import org.jooq.Configuration;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.database.model.generated.tables.daos.DisciplineDao;
import de.ithappens.run2.database.model.generated.tables.pojos.Discipline;

public class Discipline_engine extends EngineInterface<Discipline> {

	public Discipline_engine(Configuration configuration) {
		super(configuration);
	}

	@Override
	protected DisciplineDao createDao(Configuration configuration) {
		return new DisciplineDao(configuration);
	}

	@Override
	public DisciplineDao DAO() {
		return (DisciplineDao) super.DAO();
	}

	@Override
	public void update(Discipline discipline) {
		if (discipline != null) {
			discipline.setVersion(discipline.getVersion() + 1);
			discipline.setChangedate(new Timestamp(System.currentTimeMillis()));
			discipline.setChanger(UserContext.getInstance().getOperatingSystemEnvironment().getUsername());
			DAO().update(discipline);
		}
	}

	@Override
	public List<Discipline> findAll() {
		return DAO().findAll();
	}

}
