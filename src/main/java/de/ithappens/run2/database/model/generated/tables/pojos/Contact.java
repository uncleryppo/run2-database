/*
 * This file is generated by jOOQ.
 */
package de.ithappens.run2.database.model.generated.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Contact implements Serializable {

    private static final long serialVersionUID = -1864757537;

    private Integer   id;
    private String    lastname;
    private String    firstname;
    private String    callname;
    private String    phonenumber;
    private String    mobilenumber;
    private Integer   birthyear;
    private String    email;
    private String    address1;
    private String    address2;
    private String    city;
    private Integer   postal;
    private String    gender;
    private String    comments;
    private String    importFingerprint;
    private Timestamp creationdate;
    private String    creator;
    private Timestamp changedate;
    private String    changer;
    private Integer   version;
    private String    shortNameCertification;

    public Contact() {}

    public Contact(Contact value) {
        this.id = value.id;
        this.lastname = value.lastname;
        this.firstname = value.firstname;
        this.callname = value.callname;
        this.phonenumber = value.phonenumber;
        this.mobilenumber = value.mobilenumber;
        this.birthyear = value.birthyear;
        this.email = value.email;
        this.address1 = value.address1;
        this.address2 = value.address2;
        this.city = value.city;
        this.postal = value.postal;
        this.gender = value.gender;
        this.comments = value.comments;
        this.importFingerprint = value.importFingerprint;
        this.creationdate = value.creationdate;
        this.creator = value.creator;
        this.changedate = value.changedate;
        this.changer = value.changer;
        this.version = value.version;
        this.shortNameCertification = value.shortNameCertification;
    }

    public Contact(
        Integer   id,
        String    lastname,
        String    firstname,
        String    callname,
        String    phonenumber,
        String    mobilenumber,
        Integer   birthyear,
        String    email,
        String    address1,
        String    address2,
        String    city,
        Integer   postal,
        String    gender,
        String    comments,
        String    importFingerprint,
        Timestamp creationdate,
        String    creator,
        Timestamp changedate,
        String    changer,
        Integer   version,
        String    shortNameCertification
    ) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.callname = callname;
        this.phonenumber = phonenumber;
        this.mobilenumber = mobilenumber;
        this.birthyear = birthyear;
        this.email = email;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.postal = postal;
        this.gender = gender;
        this.comments = comments;
        this.importFingerprint = importFingerprint;
        this.creationdate = creationdate;
        this.creator = creator;
        this.changedate = changedate;
        this.changer = changer;
        this.version = version;
        this.shortNameCertification = shortNameCertification;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getCallname() {
        return this.callname;
    }

    public void setCallname(String callname) {
        this.callname = callname;
    }

    public String getPhonenumber() {
        return this.phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getMobilenumber() {
        return this.mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public Integer getBirthyear() {
        return this.birthyear;
    }

    public void setBirthyear(Integer birthyear) {
        this.birthyear = birthyear;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPostal() {
        return this.postal;
    }

    public void setPostal(Integer postal) {
        this.postal = postal;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImportFingerprint() {
        return this.importFingerprint;
    }

    public void setImportFingerprint(String importFingerprint) {
        this.importFingerprint = importFingerprint;
    }

    public Timestamp getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(Timestamp creationdate) {
        this.creationdate = creationdate;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Timestamp getChangedate() {
        return this.changedate;
    }

    public void setChangedate(Timestamp changedate) {
        this.changedate = changedate;
    }

    public String getChanger() {
        return this.changer;
    }

    public void setChanger(String changer) {
        this.changer = changer;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getShortNameCertification() {
        return this.shortNameCertification;
    }

    public void setShortNameCertification(String shortNameCertification) {
        this.shortNameCertification = shortNameCertification;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Contact (");

        sb.append(id);
        sb.append(", ").append(lastname);
        sb.append(", ").append(firstname);
        sb.append(", ").append(callname);
        sb.append(", ").append(phonenumber);
        sb.append(", ").append(mobilenumber);
        sb.append(", ").append(birthyear);
        sb.append(", ").append(email);
        sb.append(", ").append(address1);
        sb.append(", ").append(address2);
        sb.append(", ").append(city);
        sb.append(", ").append(postal);
        sb.append(", ").append(gender);
        sb.append(", ").append(comments);
        sb.append(", ").append(importFingerprint);
        sb.append(", ").append(creationdate);
        sb.append(", ").append(creator);
        sb.append(", ").append(changedate);
        sb.append(", ").append(changer);
        sb.append(", ").append(version);
        sb.append(", ").append(shortNameCertification);

        sb.append(")");
        return sb.toString();
    }
}
