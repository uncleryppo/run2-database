/*
 * This file is generated by jOOQ.
 */
package de.ithappens.run2.database.model.generated.tables;


import de.ithappens.run2.database.model.generated.Keys;
import de.ithappens.run2.database.model.generated.Y3jrun;
import de.ithappens.run2.database.model.generated.tables.records.DisciplineRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Discipline extends TableImpl<DisciplineRecord> {

    private static final long serialVersionUID = -643077628;

    /**
     * The reference instance of <code>Y3JRUN.DISCIPLINE</code>
     */
    public static final Discipline DISCIPLINE = new Discipline();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DisciplineRecord> getRecordType() {
        return DisciplineRecord.class;
    }

    /**
     * The column <code>Y3JRUN.DISCIPLINE.ID</code>.
     */
    public final TableField<DisciplineRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.NAME</code>.
     */
    public final TableField<DisciplineRecord, String> NAME = createField("NAME", org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.LENGTH</code>.
     */
    public final TableField<DisciplineRecord, String> LENGTH = createField("LENGTH", org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.START</code>.
     */
    public final TableField<DisciplineRecord, Timestamp> START = createField("START", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.DISCIPLINE_TIMEFORMAT</code>.
     */
    public final TableField<DisciplineRecord, String> DISCIPLINE_TIMEFORMAT = createField("DISCIPLINE_TIMEFORMAT", org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.PRICE_IN_EUROCENT</code>.
     */
    public final TableField<DisciplineRecord, Integer> PRICE_IN_EUROCENT = createField("PRICE_IN_EUROCENT", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.IMPORT_FINGERPRINT</code>.
     */
    public final TableField<DisciplineRecord, String> IMPORT_FINGERPRINT = createField("IMPORT_FINGERPRINT", org.jooq.impl.SQLDataType.VARCHAR(500), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.CREATIONDATE</code>.
     */
    public final TableField<DisciplineRecord, Timestamp> CREATIONDATE = createField("CREATIONDATE", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.CREATOR</code>.
     */
    public final TableField<DisciplineRecord, String> CREATOR = createField("CREATOR", org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.CHANGEDATE</code>.
     */
    public final TableField<DisciplineRecord, Timestamp> CHANGEDATE = createField("CHANGEDATE", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.CHANGER</code>.
     */
    public final TableField<DisciplineRecord, String> CHANGER = createField("CHANGER", org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.VERSION</code>.
     */
    public final TableField<DisciplineRecord, Integer> VERSION = createField("VERSION", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>Y3JRUN.DISCIPLINE.SHORT_NAME</code>.
     */
    public final TableField<DisciplineRecord, String> SHORT_NAME = createField("SHORT_NAME", org.jooq.impl.SQLDataType.VARCHAR(6), this, "");

    /**
     * Create a <code>Y3JRUN.DISCIPLINE</code> table reference
     */
    public Discipline() {
        this(DSL.name("DISCIPLINE"), null);
    }

    /**
     * Create an aliased <code>Y3JRUN.DISCIPLINE</code> table reference
     */
    public Discipline(String alias) {
        this(DSL.name(alias), DISCIPLINE);
    }

    /**
     * Create an aliased <code>Y3JRUN.DISCIPLINE</code> table reference
     */
    public Discipline(Name alias) {
        this(alias, DISCIPLINE);
    }

    private Discipline(Name alias, Table<DisciplineRecord> aliased) {
        this(alias, aliased, null);
    }

    private Discipline(Name alias, Table<DisciplineRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Discipline(Table<O> child, ForeignKey<O, DisciplineRecord> key) {
        super(child, key, DISCIPLINE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Y3jrun.Y3JRUN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<DisciplineRecord, Integer> getIdentity() {
        return Keys.IDENTITY_DISCIPLINE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<DisciplineRecord> getPrimaryKey() {
        return Keys.SQL120425203641050;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<DisciplineRecord>> getKeys() {
        return Arrays.<UniqueKey<DisciplineRecord>>asList(Keys.SQL120425203641050);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Discipline as(String alias) {
        return new Discipline(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Discipline as(Name alias) {
        return new Discipline(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Discipline rename(String name) {
        return new Discipline(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Discipline rename(Name name) {
        return new Discipline(name, null);
    }
}
